function problem4(nameObject){
    let fullName = "";
    for(let key of Object.keys(nameObject)){
        let name = nameObject[key];
        fullName += name.charAt(0).toUpperCase().concat(name.slice(1,name.length).toLowerCase()," ");
    }
    return fullName;
}

module.exports = problem4;