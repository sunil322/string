function problem1(str){
    str=str.replace("$","");
    str=str.replaceAll(",","");
    const result = Number(str);
    if(isNaN(result)){
        return 0;
    }else{
        return result;
    }
}

module.exports = problem1;