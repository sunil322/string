function problem5(arrayOfString){
    let str = "";
    if(Array.isArray(arrayOfString)){
        if(arrayOfString.length === 0){
            return str;
        }else{
            for(let index=0;index<arrayOfString.length;index++){
                str = str.concat(arrayOfString[index]," ");
            }
            return str;
        }
    }else{
        return str;
    }
}

module.exports = problem5;
