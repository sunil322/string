function problem2(str){
    let splitAddress = str.split(".");
    let addressInNumeric = [];
    for(let index=0;index<splitAddress.length;index++){
        if(isNaN(splitAddress[index])){
            return [];
        }else{
            addressInNumeric.push(Number(splitAddress[index]));
        }
    }
    return addressInNumeric;
}

module.exports = problem2;

